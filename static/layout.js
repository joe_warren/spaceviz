function paramString(params){
  var str = "";
  var amp = "";
  for(var p in params){
    if(params.hasOwnProperty(p)) {
        str += amp + encodeURIComponent(p) + "=" + encodeURIComponent(params[p]);
        amp = "&";
    }
  }
  return str;
}

Array.prototype.flatMap = function(lambda) { 
    return Array.prototype.concat.apply([], this.map(lambda)); 
};

function drawParseTree(data){
  var svg = d3.select("#svg");
  svg.selectAll("*").remove();
  var width = 800;
  var height = 600;
  var roots = data.map(d=>d3.hierarchy(d));
  var nodes = roots.flatMap(r=>r.descendants())
  var links = roots.flatMap(r=>r.links())
  

  var xPos = function(d){
    if(d.data.i !== undefined){
      return d.data.i;
    }
    d.sum(function(d){ return d.i || 0 });
    var total = d.value;
    d.count();
    return total/d.value
  };

  var maxI = Math.max.apply(null,nodes.map(xPos));
  var maxDepth = Math.max.apply(null,nodes.map(d=>d.depth));

  var xStep = width/(maxI+1);
  var yStep = height/(maxDepth+1);
  var x = function(d){ return (xPos(d)+0.5)*xStep };
  var y = function(d){ return (d.depth +0.5)*yStep};
 

  var nodeGroups = svg.selectAll(".nodes").data(nodes).enter()
    .append("g").classed("nodes", true)
    .attr("transform", function(d){
      return "translate( " + x(d) + "," + y(d) + ")";
    });
  nodeGroups.append("text").text(function(d){return d.data.tag})
  nodeGroups.filter(function(d){ return d.data.val })
    .append("text")
      .classed("content", true)
      .attr("transform", "translate(0, 25)")
      .text(function(d){return d.data.val});

  var diagonal = function(d){
    p = d3.path();
    p.moveTo(x(d.source), y(d.source));
    var midy = (y(d.source) + y(d.target))/2;
    p.bezierCurveTo( x(d.source), midy,
      x(d.target), midy,
      x(d.target), y(d.target));
   return p.toString()
  }

  svg.selectAll(".links").data(links).enter()
    .append("path").classed("links", true)
    .attr("d", diagonal);
  window.console.log(nodes)
}

function handleSubmit(){
  var text = document.getElementById("text").value
  d3.request("/tree.json")
    .header("X-Requested-With", "XMLHttpRequest")
    .header("Content-Type", "application/x-www-form-urlencoded")
    .post(paramString({"text": text}), function(result){
        var data = JSON.parse(result.response);
        window.console.log(data);
        drawParseTree(data);
  });
  return false;
}
