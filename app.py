#!/usr/bin/env python
import parse
from flask import Flask, jsonify, request
app = Flask(__name__)


@app.route("/")
def index():
    return app.send_static_file('index.html')

@app.route('/tree.json', methods=["POST"])
def join():
    if request.method == 'POST':
        text = request.form['text']
        return jsonify(parse.tree(text))
    else:
        raise Exception("Invalid method")

if __name__ == "__main__":
    app.run()
