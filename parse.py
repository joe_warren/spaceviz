#!/usr/bin/env python

import spacy                          
import json
nlp = spacy.load('en')                

def leaf(node):
    return {
      "tag":str(node.pos_),
      "val":node.orth_,
      "i":node.i
    }

def sentenceAsTree(node):
   children = list(node.children)
   if len(children) != 0:
     return {
      "tag":str(node.dep_),
      "children":[sentenceAsTree(c) for c in children]+[leaf(node)]
     }
   else:
     return leaf(node)

def tree(text):
    doc = nlp(text)
    return [sentenceAsTree(s.root) for s in doc.sents]

if __name__ == "__main__":
    import sys
    print(json.dumps(tree(u" ".join(sys.argv[1:])), indent=2))
